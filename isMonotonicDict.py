# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 19:45:26 2021

@author: Nicolau
"""

def isMonotonic(array):
    dic = {}
    aux = 0
    for i in range(len(array)-1):
        if array[i] > array[i+1]:
            dic['A'] = True
        if array[i] < array[i+1]: 
            dic['B'] = True
    print(dic)
    if 'A' in dic:
        aux += 1
    if 'B' in dic:
        aux += 1
    if aux <= 1:
        return True
    else:
        return False

a = [1, 5, 10, 1100, 1101, 1102, 9001]

print(isMonotonic(a))

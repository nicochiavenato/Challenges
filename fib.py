# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 17:34:07 2021

@author: Nicolau
"""

def getNthFib(n):
    if n == 2:
		return 1
	elif n == 1:
        return 0
    else:
        return getNthFib(n-1) + getNthFib(n-2)

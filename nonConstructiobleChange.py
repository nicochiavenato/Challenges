# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 17:55:28 2021

@author: Nicolau
"""

def nonConstructibleChange(coins):
    coins.sort()    
    troco = 0
    for el in coins:
        if el > troco+1:
            return troco+1
        troco += el
    return troco+1

    

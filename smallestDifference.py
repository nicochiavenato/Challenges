# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 19:48:13 2021

@author: Nicolau
"""

def smallestDifference(arrayOne, arrayTwo):
    for i in range(len(arrayOne)):
        for j in range(len(arrayTwo)):
            aux = -arrayOne[i] + arrayTwo[j]
            if aux == 0:
                return [arrayOne[i],arrayTwo[j]]
            if i == 0 and j == 0:
                dif = -arrayOne[i] + arrayTwo[j]
            if abs(aux) < abs(dif):
                dif = aux
                res = [arrayOne[i],arrayTwo[j]]
    return res
    
arrayOne = [-1, 5, 10, 20, 28, 3]
arrayTwo = [26, 134, 135, 15, 17]

print(smallestDifference(arrayOne, arrayTwo))

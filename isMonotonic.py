# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 19:23:08 2021

@author: Nicolau
"""

def isMonotonic(array):
    desc = False
    if len(array) == 0:
        return True
    if len(array) == 1:
        return True   
    for i in range(len(array)-1):
        if i == 0:
            if array[i] > array[i+1]:
                desc = True
        else:
            if desc:
                if array[i] >= array[i+1]:
                    if i == len(array)-2:
                        return True
                    else:
                        pass
                else:
                    return False
            else:
                if array[i] <= array[i+1]:
                    if i == len(array)-2:
                        return True
                    else:
                        pass
                else:
                    return False                

a = [-1, -1, -2, -3, -4, -5, -5, -5, -6, -7, -8, -8, -9, -10, -11]

print(isMonotonic(a))

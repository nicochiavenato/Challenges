# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 19:16:07 2021

@author: Nicolau
"""

def threeNumberSum(array, targetSum):
    # Write your code here.
    dictA = {}
    x = 0
    resposta = []
    array.sort()
    l = len(array)
    ini = 1
    for j in range(l):
        for z in range(ini,l):
            if array[j] not in dictA:
                dictA[array[j]] = True
            if array[z] not in dictA:
                dictA[array[z]] = True
            x = targetSum-array[j]-array[z]
            if x in dictA and array[j]!=array[z] and array[z]!=x and array[j] != x:
                resposta.append([array[j],x,array[z]])
        ini += 1
    return resposta

array = [12, 3, 1, 2, -6, 5, -8, 6]
targetSum = 0

print(threeNumberSum(array, targetSum))

# [[-8, 2, 6], [-8, 3, 5], [-6, 1, 5]]
                
    

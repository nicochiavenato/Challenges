# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 15:08:04 2021

@author: Nicolau
"""

def tournamentWinner(competitions, results):
    auxIndex = 0
    dictResults = {}
    maior = 0
    resultado = ''
    for index,el in enumerate(results):
        if el == 0:
            results[index] = 1
        else:
            results[index] = 0
    for el in competitions:
        if el[results[auxIndex]] in dictResults:
            dictResults[el[results[auxIndex]]] += 1
        else:
            dictResults[el[results[auxIndex]]] = 1
        auxIndex += 1
    for el in dictResults:
        if dictResults[el] > maior:
            maior = dictResults[el]
            resultado = el
    return resultado

competitions = [
    ["HTML", "Java"],
    ["Java", "Python"],
    ["Python", "HTML"]
  ]
results = [0, 1, 1]

print(tournamentWinner(competitions, results))

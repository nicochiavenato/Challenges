# -*- coding: utf-8 -*-
"""
Created on Sat Jul 31 12:41:08 2021

@author: Nicolau
"""

array = [3,5,-4,8,11,10,-1,6]
targetSum = 10

def twoNumberSum(array,targetSum):
    resto = 0
    for el in array:
        resto = targetSum - el
        for elDois in array:
            if elDois == resto and el != elDois:
                return [el,elDois]
    return []

def twoNumberSumD(array,targetSum):
    dictAuxiliar = {}
    resto = 0
    for el in array:
        dictAuxiliar[el] = True
        resto = targetSum - el
        if el != resto and resto in dictAuxiliar:
            return [resto,el]
    return []
            
def threeNumerSum(array,targetSum):
    dictA = {}
    restoA = 0
    restoB = 0
    for el in array:
        restoA = targetSum - el
        for elDois in array:
            restoB = restoA - restoB
            
    

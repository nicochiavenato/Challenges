# -*- coding: utf-8 -*-
"""
Created on Sun Aug  1 21:52:53 2021

@author: Nicolau
"""

def isValidSubsequence(array, sequence):
    indexAnterior = 0
    somaAux = 0
    for index,el in enumerate(sequence):
        if el in array and array.index(el) >= indexAnterior:
            somaAux += 1
            indexAnterior += 1
            array[array.index(el)] = 'visto'
        if somaAux == len(sequence):
            return True
    return False

array = [1, 1, 1, 1, 1]
sequence = [1, 1, 1]

print(isValidSubsequence(array,sequence))

def teste(array,sequence):
    aux = 0
    for el in array:
        if aux == len(sequence):
            break
        if sequence[aux] == el:
            aux += 1
    return aux == len(sequence)

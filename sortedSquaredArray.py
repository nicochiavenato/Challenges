# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 12:30:10 2021

@author: Nicolau
"""

def sortedSquaredArray(array):
    elSquare = 0
    newArray = []
    for el in array:
        elSquare = el * el
        newArray.append(elSquare)
    newArray.sort()
    return newArray

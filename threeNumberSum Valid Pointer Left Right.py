# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 19:42:14 2021

@author: Nicolau
"""

def threeNumberSum(array, targetSum):
    array.sort()
    resposta = []
    for i in range(len(array)-2):
        left = i+1
        right = len(array)-1
        while left < right:
            somaAtual = array[left] + array[right] + array[i]
            if somaAtual == targetSum:
                resposta.append([array[i],array[left],array[right]])
                left += 1
                right -= 1
            elif somaAtual < targetSum:
                left += 1
            elif somaAtual > targetSum:
                right -= 1
    return resposta

array = [12, 3, 1, 2, -6, 5, -8, 6]
targetSum = 0

print(threeNumberSum(array, targetSum))

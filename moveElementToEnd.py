# -*- coding: utf-8 -*-
"""
Created on Sun Aug  8 23:02:59 2021

@author: Nicolau
"""

def moveElementToEnd(array, toMove):
    right = len(array)-1
    left = 0
    if len(array) == 0:
        return array
    while right > left:
        if array[left] == toMove and array[right] == toMove:
            right -= 1
        elif array[left] == toMove and array[right] != toMove:
            array[left], array[right] = array[right], array[left]
            left += 1
            right -= 1
        else:
            left += 1
    return array

a = [2, 4, 2, 5, 6, 2, 2]
x = 2

print(moveElementToEnd(a,x))
            
            
